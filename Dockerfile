FROM listenup/listenup_common:v0.38

COPY requirements.txt /usr/src/app/
RUN pip install -r requirements.txt

ARG SERVICE_NAME
ARG SERVICE_VERSION
ENV SERVICE_NAME $SERVICE_NAME
ENV SERVICE_VERSION $SERVICE_VERSION

COPY . ${appRoot}

RUN ./fix_celery.sh && mkdir -p temp && chown nobody temp && chmod -R 777 temp

USER nobody

EXPOSE 5000

# Start gunicorn
CMD ["./entrypoint.sh"]
