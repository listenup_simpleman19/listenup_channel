from typing import List
from flask import current_app as app
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from listenup_common.database import JsonMixin
import uuid

db = SQLAlchemy()


def guid():
    return str(uuid.uuid4()).replace('-', '')


class BaseModel(JsonMixin, db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    creation_timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


class Channel(BaseModel):
    __tablename_ = 'channel'
    guid = db.Column(db.String(32), nullable=False, unique=True)
    created_by_user_guid = db.Column(db.String(32), nullable=False)
    from_rss_feed = db.Column(db.Boolean, nullable=False, default=False)
    feed_url = db.Column(db.String(400), nullable=True, unique=True)

    title = db.Column(db.String(400), nullable=True)
    description = db.Column(db.String(10000), nullable=True)
    link = db.Column(db.String(400), nullable=True)

    language = db.Column(db.String(100), nullable=True)
    category = db.Column(db.String(500), nullable=True)
    pub_date = db.Column(db.DateTime, nullable=True)
    last_build_date = db.Column(db.DateTime, nullable=True)

    image_url = db.Column(db.String(400), nullable=True)
    image_title = db.Column(db.String(400), nullable=True)
    image_link = db.Column(db.String(400), nullable=True)
    image_guid = db.Column(db.String(32), nullable=True)

    itunes_subtitle = db.Column(db.String(10000), nullable=True)
    itunes_image_url = db.Column(db.String(400))
    itunes_author = db.Column(db.String(400))

    owners = db.relationship("ChannelOwner", back_populates="channel")
    podcasts = db.relationship("Podcast", back_populates="channel")

    _default_fields = [
        "title",
        "description",
        "created_by_user_guid",
        "link",
        "language",
        "category",
        "pub_date",
        "last_build_date",
        "image_url",
        "image_title",
        "image_link",
        "image_guid",
        "itunes_subtitle",
        "itunes_image_url",
        "itunes_author",
        "from_rss_feed",
        "feed_url",
    ]

    _readonly_fields = [
        "created_by_user_guid",
        "from_rss_feed",
        "feed_url",
    ]

    def __init__(self, user_guid=None, **kwargs):
        super().__init__(**kwargs)
        self.guid = guid()
        self.created_by_user_guid = user_guid


class ChannelOwner(BaseModel):
    __tablename__ = 'channel_owner'
    guid = db.Column(db.String(32), nullable=False)
    channel_id = db.Column(db.Integer, db.ForeignKey('channel.id'), nullable=False)
    channel = db.relationship("Channel", back_populates="owners")
    user_guid = db.Column(db.String(32), nullable=False)

    _default_fields = [
        "guid",
        "user_guid"
    ]

    _readonly_fields = [
        "channel_id",
        "channel",
        "user_guid",
    ]

    def __init__(self, channel: Channel = None, user_guid: str = None, **kwargs):
        super().__init__(**kwargs)
        self.guid = guid()
        self.user_guid = user_guid
        if channel:
            self.channel = channel


class Podcast(BaseModel):
    __tablename__ = 'podcast'
    guid = db.Column(db.String(32), nullable=False)
    channel_id = db.Column(db.Integer, db.ForeignKey('channel.id'), nullable=False)
    channel = db.relationship("Channel", back_populates="podcasts")
    podcast_guid = db.Column(db.String(32), nullable=False)

    _default_fields = [
        "guid",
        "podcast_guid"
    ]

    _readonly_fields = [
        "channel_id",
        "channel",
        "podcast_guid",
    ]

    def __init__(self, channel: Channel, podcast_guid: str, **kwargs):
        super().__init__(**kwargs)
        self.guid = guid()
        self.channel = channel
        self.podcast_guid = podcast_guid
