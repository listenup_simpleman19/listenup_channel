import os
from typing import List
from flask import Blueprint, render_template, session, redirect, url_for, request, current_app as app, g
from listenup_common.auth import token_auth, token_optional_auth
from listenup_common.api_response import ApiException, ApiMessage, ApiResult
from listenup_common.wrappers.podcast import get_podcasts_info_by_guids, add_owner_to_podcast, remove_owner_from_podcast
from listenup_common.utils import require_json
from listenup_common.logging import get_logger
from channel.tasks import test_task
from channel.models import ChannelOwner, Channel, Podcast
from . import db

logger = get_logger(__name__)

main = Blueprint('main', __name__, url_prefix='/api/channel')

root_path = os.path.dirname(os.path.abspath(__file__))


@main.route("/test")
def test():
    test_task.apply_async(args=[], countdown=15)
    return ApiResult(value={"hi": "this is a test"}, status=200).to_response()


@main.route("/add", methods=["POST"])
@token_auth.login_required
@require_json()
def add_channel(json):
    logger.info("Creating a new channel")
    channel = Channel(user_guid=g.current_user_guid, **json)
    channel.owners.append(ChannelOwner(user_guid=g.current_user_guid))
    db.session.add(channel)
    db.session.commit()
    logger.info("Created channel: %s", channel.guid)
    return ApiResult(value=channel.to_dict(show=["owners"]), status=200).to_response()


@main.route("/all", methods=["GET"])
@token_optional_auth.login_required
def get_channel_all():
    logger.warn("Getting all channels")
    channels = Channel.query.all()
    return ApiResult(value=[channel.to_dict(show=["owners"]) for channel in channels], status=200).to_response()


@main.route("/<string:guid>", methods=["GET"])
@token_optional_auth.login_required
def get_channel(guid):
    logger.debug("Getting channel for guid: %s", guid)
    channel = Channel.query.filter_by(guid=guid).one_or_none()
    if channel:
        return ApiResult(value=channel.to_dict(show=["owners", "podcasts"]), status=200).to_response()
    else:
        raise ApiException(message="Could not find channel for " + guid, status=404)


@main.route("/<string:guid>", methods=["PUT"])
@token_optional_auth.login_required
@require_json()
def update_channel(guid, json):
    logger.debug("Getting channel for guid: %s", guid)
    channel = Channel.query.filter_by(guid=guid).one_or_none()
    if channel:
        channel.from_dict(json)
        db.session.add(channel)
        db.session.commit()
        return ApiResult(value=channel.to_dict(show=["owners"]), status=200).to_response()
    else:
        raise ApiException(message="Could not find channel for " + guid, status=404)


@main.route("/<string:guid>/owners", methods=["GET"])
@token_optional_auth.login_required
def get_owners(guid):
    logger.info("Getting owners for channel: %s", guid)
    channel: Channel = Channel.query.filter_by(guid=guid).one_or_none()
    if channel:
        return ApiResult(value=[owner.to_dict() for owner in channel.owners], status=200).to_response()
    else:
        raise ApiException(message="Could not find channel for guid: " + guid, status=404)


@main.route("/<string:guid>/podcasts", methods=["GET"])
@token_optional_auth.login_required
def get_podcasts(guid):
    logger.info("Getting podcast for channel: %s", guid)
    channel = Channel.query.filter_by(guid=guid).one_or_none()
    if not channel:
        raise ApiException(message="Could not find channel for guid: " + guid, status=404)
    podcasts = get_podcasts_info_by_guids(guids=[podcast.podcast_guid for podcast in channel.podcasts])
    if not podcasts:
        raise ApiException(message="No podcasts found for channel: " + guid, status=404)
    return ApiResult(value={'podcasts': podcasts}, status=200).to_response()


@main.route("/<string:guid>/owner", methods=["POST"])
@token_auth.login_required
@require_json(required_attrs=["user_guid"])
def add_owner(guid, json):
    logger.info("Adding owner to channel: %s", guid)
    user_guid = json['user_guid']
    if not validate_user(user_guid):
        raise ApiException(message="Invalid user guid", status=404)
    channel = Channel.query.filter_by(guid=guid).one_or_none()
    if not ChannelOwner.query.filter_by(channel=channel, user_guid=g.current_user_guid).one_or_none():
        raise ApiException(message="Ownership can not be changed if you are not an owner of the channel", status=401)
    # TODO all probably doesn't make sense but there are no constraints right now for channel+user to be unique
    owner = ChannelOwner.query.filter_by(channel=channel, user_guid=user_guid).all()
    if not channel:
        raise ApiException(message="Could not find channel for guid: " + guid, status=404)
    if owner:
        raise ApiException(message="User is already an owner")

    new_owner = ChannelOwner(channel=channel, user_guid=user_guid)
    db.session.add(new_owner)
    db.session.commit()

    for podcast in channel.podcasts:
        add_owner_to_podcast(podcast.podcast_guid, user_guid)
    return get_channel(guid)


@main.route("/<string:guid>/owner", methods=["DELETE"])
@token_auth.login_required
@require_json(required_attrs=["user_guid"])
def delete_owner(guid, json):
    logger.info("Deleting owner from channel: %s", guid)
    if not ChannelOwner.query.filter_by(channel_guid=guid, user_guid=g.current_user_guid).one_or_none():
        raise ApiException(message="Ownership can not be changed if you are not an owner of the channel", status=401)
    channel = Channel.query.filter_by(guid=guid).one_or_none()
    if len(channel.owners) == 1:
        raise ApiException(message="Only one owner left, channel must have atleast one owner")
    owners = ChannelOwner.query.filter_by(channel_guid=guid, user_guid=json['user_guid']).all()
    if owners:
        for owner in owners:
            db.session.delete(owner)
        db.session.commit()
    return ApiMessage(message="Removed owner", status=200).to_response()


@main.route("/<string:guid>/podcast", methods=["POST"])
@token_auth.login_required
@require_json(required_attrs=["podcast_guid"])
def add_podcast(guid, json):
    if not validate_podcast(json['podcast_guid']):
        raise ApiException(message="Invalid user guid", status=404)
    # TODO check if requesting user is an owner...
    channel = Channel.query.filter_by(guid=guid).one_or_none()
    # TODO all probably doesn't make sense but there are no constraints right now for channel+podcast to be unique
    podcast = Podcast.query.filter_by(channel=channel, podcast_guid=json['podcast_guid']).all()
    if not channel:
        raise ApiException(message="Could not find channel for guid: " + guid, status=404)
    if podcast:
        raise ApiException(message="Podcast: " + json['podcast_guid'] + " already exists for channel: " + guid)

    new_podcast = Podcast(channel=channel, podcast_guid=json['podcast_guid'])
    db.session.add(new_podcast)
    db.session.commit()

    for owner in channel.owners:
        add_owner_to_podcast(json['podcast_guid'], owner.user_guid)
    return get_channel(guid)


@main.route("/<string:guid>/podcast", methods=["DELETE"])
@token_auth.login_required
@require_json(required_attrs=["podcast_guid"])
def delete_podcast(guid, json):
    # TODO check for only 1 owner left and prevent deletion
    podcasts = Podcast.query.filter_by(channel_guid=guid, podcast_guid=json['podcast_guid']).all()
    if podcasts:
        for podcast in podcasts:
            db.session.delete(podcast)
        db.session.commit()
    return ApiMessage(message="Removed podcast", status=200).to_response()


@main.route("/rss", methods=["GET"])
@token_optional_auth.login_required
def get_channel_all_rss():
    logger.warn("Getting all channels from rss")
    channels = Channel.query.filter_by(from_rss_feed=True).all()
    return ApiResult(
        value=[channel.to_dict(show=["owners"]) for channel in channels],
        status=200).to_response()


@main.route("/user/<string:guid>", methods=["GET"])
def get_channels_by_user(guid):
    # TODO joins...
    channel_owners: List[ChannelOwner] = ChannelOwner.query.filter_by(user_guid=guid).all()
    if channel_owners:
        return ApiResult(value={"channels": [channel_owner.channel.to_dict() for channel_owner in channel_owners]}).to_response()
    else:
        raise ApiException(message="Could not find channels for user: " + guid)


@main.route("/podcast/<string:guid>", methods=["GET"])
def get_channel_by_podcast(guid):
    podcast: Podcast = Podcast.query.filter_by(podcast_guid=guid).one_or_none()
    if podcast and podcast.channel:
        return ApiResult(value={"channel": podcast.channel.to_dict()}).to_response()
    else:
        raise ApiException(message="Could not find channel for podcast: " + guid)


@main.route("/url", methods=["GET"])
@token_optional_auth.login_required
@require_json(required_attrs=["feed_url"])
def get_channel_by_url(json):
    channel = Channel.query.filter_by(feed_url=json.get("feed_url")).one_or_none()
    if channel:
        return ApiResult(value=channel.to_dict(), status=200).to_response()
    else:
        raise ApiException(message="Could not find channel for url: " + json.get("feed_url", "N/A"), status=404)


@main.route("/categories", methods=["get"])
def get_categories():
    return ApiResult(value={
        "categories": ["business", "technology", "other"]
    }).to_response()


def validate_user(guid: str) -> bool:
    # TODO actually validate user
    return True


def validate_podcast(guid: str) -> bool:
    # TODO actually validate podcast
    return True


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
