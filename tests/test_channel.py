import unittest
from channel import create_app
from channel.models import db, Channel, ChannelOwner
import pprint
from listenup_common.test import FlackTestCase
import os

pp = pprint.PrettyPrinter(indent=4)


class TestRSS(FlackTestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.ctx = self.app.app_context()
        self.ctx.push()
        db.session.commit()
        db.drop_all()
        db.create_all()
        self.client = self.app.test_client()

    def test_add(self):
        channel = Channel(user_guid="A")
        channel.owners.append(ChannelOwner("A"))
        db.session.add(channel)
        db.session.commit()
        channel = Channel.query.filter_by(guid=channel.guid).one_or_none()
        self.assertEqual("A", channel.created_by_user_guid)
        channel_dict = channel.to_dict(show=["owners"])
        pp.pprint(channel_dict)
        self.assertEqual("A", channel_dict["created_by_user_guid"])
        self.assertEqual(1, len(channel_dict["owners"]))

    def test_add_multi(self):
        for x in range(0, 10):
            print("Channel: " + str(x))
            user_guid = "A" + str(x)
            channel = Channel(user_guid=user_guid)
            channel.owners.append(ChannelOwner(user_guid=user_guid))
            db.session.add(channel)
            db.session.commit()
            channel = Channel.query.filter_by(guid=channel.guid).one_or_none()
            self.assertEqual(user_guid, channel.created_by_user_guid)
            channel_dict = channel.to_dict(show=["owners"])
            pp.pprint(channel_dict)
            self.assertEqual(user_guid, channel_dict["created_by_user_guid"])
            self.assertEqual(1, len(channel_dict["owners"]))

    def test_add_json(self):
        json = {
            "title": "A Title",
            "description": "A Description",
            "language": "English"
        }
        channel2 = Channel(user_guid="B", **json)
        channel2.owners.append(ChannelOwner("B"))
        db.session.add(channel2)
        db.session.commit()
        channel2 = Channel.query.filter_by(guid=channel2.guid).one_or_none()
        self.assertEqual("B", channel2.created_by_user_guid)
        channel_dict = channel2.to_dict(show=["language", "owners"])
        pp.pprint(channel_dict)
        self.assertEqual("B", channel_dict["created_by_user_guid"])
        self.assertEqual(json["title"], channel_dict["title"])
        self.assertEqual(json["description"], channel_dict["description"])
        self.assertEqual(json["language"], channel_dict["language"])
